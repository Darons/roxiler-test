import { Component, OnInit } from '@angular/core';
import { TodoService } from './services/todo.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private ts: TodoService) {}
  todos: Array<any> = new Array<any>();
  user: any;
  ascending: boolean = true;

  /**
   * For data models, I like to create classes defining the attributes, matching the json structure,
   * so instead of any I would type correctly each model.
   */ 
  ngOnInit() {
    this.ts.getTodos().subscribe((res) => {
      this.todos = res;
    });
  }

  showUser(todo: any) {
    this.ts.getUser(todo.userId).subscribe((res) => {
      let userTodos = this.todos.filter((to) => {
        return to.userId == todo.userId;
      });
      console.log(userTodos);
      this.user = {
        title: todo.title,
        id: res.id,
        email: res.email,
        name: res.name,
        todoId: todo.id,
        todos: userTodos,
      };
    });
  }

  sortTable() {
    this.ascending
      ? (this.todos = this.todos.sort((a, b) => {
          return b.id - a.id;
        }))
      : (this.todos = this.todos.sort((a, b) => {
          return a.id - b.id;
        }));
    this.ascending = !this.ascending;
  }
}
